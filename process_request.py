#!/usr/bin/env python

"""
Process request file by digging info about ip addresses,
termination of subnetworks, interfaces, access-lists
Make a suggestion of addition to access-list
Script trying to guess vpn in wich subnet is originated,
but it can be overrided by param <proposal-vpn>
"""

#import re
import digger
import sys
import who_is_origin
import ask_origin

C_GREEN = '\033[1;32;40m'
C_NORM = '\033[0m'

def choose_common_vpn(orig_info):
	"""
	try to guess most approriate common vpn from who_is_origins info
	rtype: string or None if any common vpn doesn't exist
	"""
	uniq = []	# list of all uniqs vpns
	full = []	# full list of all vpns entries
	for (net, prefix, vpns, nexthops) in orig_info:
		full.extend(vpns)
	uniq = list(set(full))	# uniq vpns names

	if full.count('V3-BILLING03-00') == len(orig_info):
		return 'V3-BILLING03-00'
	if full.count('V20-OFFICE20-00') == len(orig_info):
		return 'V20-OFFICE20-00'

	for vpn in uniq:
		if full.count(vpn) == len(orig_info):
			return vpn


if __name__ == "__main__":

	print "usage: process_request.py [<request-file-name>] [<proposal-vpn>]"
	if len(sys.argv) > 1:
		request = digger.read_request_file(sys.argv[1])
	else:
		request = digger.read_request_file()

	digger.print_request(request)
	all_ips = digger.extract_all_ips(request)
	print
	print 'all ips  '+ (C_GREEN+' '+C_NORM).join(all_ips)
	print
	
	print 'gathering who_is_origin info...',
	orig_info = []	# origination info
	for ip in all_ips:
		orig_info.append(who_is_origin.who_is_origin(ip))
	print 'done'
	#print 
	#for info in orig_info:
	#	print info
	#print
	
	if len(sys.argv) > 2:
		common_vpn = sys.argv[2]
	else:
		common_vpn = choose_common_vpn(orig_info)
	print 'proposal vpn  '+ common_vpn
	
	subnets = []
	# list of all subnets in which all ip addresses are contained in
	for (net, prefix, vpns, nexthops) in orig_info:
		subnets.append(digger.SubNet(net, prefix, request, nexthops, vpns))
		print subnets[-1]
	print
	print 'with vrf picked:'
	for sn in subnets:
		sn.pick_vrf(common_vpn)
		print sn
	
	print 'with origination info:'
	for sn in subnets:
		hostname, iface, iface_cont, acl, acl_cont, acl_add = \
			ask_origin.ask_origin(sn)
		sn.add_orig_info( hostname, iface, iface_cont, acl, acl_cont, acl_add )
		print sn

	for sn in subnets:
		print '\nacl_addition for '+ sn.ip +'/'+ str(sn.pref) 
		print '\n'.join(sn.acl_add)

