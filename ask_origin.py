#!/usr/bin/env python
"""
acquire info from netnode about connected interface, access-list/filter
make a suggestion about access-list/filter addition configuration
"""

#import pexpect
import iplib
import re
import sys
import digger
import datetime


def parse_args():
	"""
	parse command line argument
	rtype: (string, int, string, string)
	"""
	if len(sys.argv) != 5:
		print 'usage: ask_origin <host_ip> <net_ip/prefix> <originator> <vrf>'
		print 'example, ./ask_origin.py 10.78.78.22 10.78.10.0/24 10.78.104.209 V3'
		sys.exit(0)

	match = re.search('(\d+\.\d+\.\d+\.\d+)', sys.argv[1])
	if match:
		iphost = match.group(1)
	else:
		print 'ERROR: disallowed paramemter: '+ sys.argv[1]
		sys.exit(0)

	match = re.search('(\d+\.\d+\.\d+\.\d+)\/(\d+)', sys.argv[2])
	if match:
		ipnet = match.group(1)
		prefix = int(match.group(2))
	else:
		print 'ERROR: disallowed paramemter: '+ sys.argv[2]
		sys.exit(0)

	match = re.search('(\d+\.\d+\.\d+\.\d+)', sys.argv[3])
	if match:
		originator = match.group(1)
	else:
		print 'ERROR: disallowed paramemter: '+ sys.argv[3]
		sys.exit(0)

	vrf = sys.argv[4]
	return iphost, ipnet, prefix, originator, vrf

def extract_interface_juniper(lines):
	"""
	extract interface name from list of strings in junos
	rtype: string
	"""
	for line in lines:
		match = re.search('\s+> via (\S+)\s*', line)
		if match:
			return match.group(1)

def extract_interface_cisco(lines, ipnet, prefix):
	"""
	extract interface name from list of strings in cisco
	rtype: string
	"""
#	for line in lines:
#		# some cisco output looks like this one:
#		match = re.search('C\s+'+ ipnet +'/'+ str(prefix) +\
#			' is directly connected, [dwmy0-9]*,? ?(\S+)\s*', line)
#		if match:
#			return match.group(1)

	for line in lines:
		# some cisco output looks like another one:
		match = re.search('\s+\*\s+directly connected, via [dwmy0-9]*,? ?(\S+)\s*', line)
		if match:
			return match.group(1)

	return 'iface_not_found'

def next_term(lines):
	"""
	try to guess next TERM in junos filter configuration
	rtype: string
	"""
	if len(lines) == 1:
		match = re.search('(\w+)-(\d+)', lines[0])
		if match:
			return match.group(1) +'-'+ str(int(match.group(2)) + 10)
		else: return lines[0]
	pref = 'TERM'
	next_numb = 10
	for line in lines:
		match = re.search('term (\S+)-(\d+) {', line)
		if match:
			pref = match.group(1)
			try:
				numb = int(match.group(2))
				if numb > next_numb:
					next_numb = numb
			except: pass
	return pref +'-'+ str(((next_numb / 10 ) + 1 ) * 10)

def get_origination_info(session, subnet):
	"""
	dig origination info from hostname
	rtype: (string, list, list, list)
	"""
	if session.vendor == 'juniper':
#		lines = digger.push_command(session, hostname,
#			'show route '+ subnet.ip +'/'+ str(subnet.pref) +' exact')
		lines = session.send('show route '+ subnet.ip +'/'+ str(subnet.pref) +' exact')
		interface = extract_interface_juniper(lines)
#		if_info = digger.push_command(session, hostname,	# interface information
#			'show configuration interface '+ interface)
		if_info = session.send('show configuration interface '+ interface)
		acl_info = []	# access-list information
		acl_name = ''
		for line in if_info:
			match = re.search('input (\S+);', line)
			if match:
				acl_name = match.group(1)
#				acl_info = digger.push_command(session, hostname,
#					'show configuration firewall family inet filter '+ acl_name)
				acl_info = session.send('show configuration firewall family inet filter '+ acl_name)
		sugg = []
		if acl_name != '':
			sugg = suggestion_juniper(session.hostname, acl_name, next_term(acl_info), subnet)
		return interface, if_info[:-10], acl_name, acl_info[:-2], sugg

	elif session.vendor == 'cisco':
		mask = iplib.prefix_to_mask(subnet.pref)
#		lines = digger.push_command(session, hostname,
#			'show ip route vrf '+ subnet.vrf +' '+ subnet.ip +' '+ mask )
		lines = session.send('show ip route vrf '+ subnet.vrf +' '+ subnet.ip +' '+ mask )
		interface = extract_interface_cisco(lines, subnet.ip, subnet.pref)
#		if_info = digger.push_command(session, hostname,	# interface information
#			'sh run int '+ interface)
		if_info = session.send('sh run int '+ interface)
		acl_info = []	# access-list information
		acl_name = ''
		for line in if_info:
			match = re.search('ip access-group (\S+) in', line)
			if match:
				acl_name = match.group(1)
#				acl_info = digger.push_command(session, hostname,
#					'sh ip access-lists '+ match.group(1) )
				acl_info = session.send('sh ip access-lists '+ match.group(1))
		sugg = []
		if acl_name != '':
			sugg = suggestion_cisco(session.hostname, acl_name, subnet)
		return interface, if_info[5:-3], acl_name, acl_info, sugg

	else:
		print 'ERROR unsupported vendor '+ session.vendor
		sys.exit(0)

def add_cisco_prefix_to_port(string):
	"""
	convert port info to cisco's notation
	rtype: string
	"""
	if string == '': return ''
	if string.count('-') == 1:
		return ' ra '+ string.replace('-',' ')
	if string.count('>') == 1:
		return ' gt '+ string.replace('>','').strip()
	return ' eq '+ string.strip()

def port_in_juniper_notation(string):
	"""
	convert port info to junos's notation
	rtype: string
	"""
	string = re.sub('[,;.]',' ', string)
	string = string.strip()
	match = re.search('([\d\s]*)>\s*(\d+)([\d\s]*)', string)	# whether '>' exists
	if match:
		string = ''
		if match.group(1) != '':
			string += match.group(1) +' '
		gtport = str(int(match.group(2)) + 1)
		string += gtport +'-65535'  + match.group(3)
	if string.count(' ') != 0:
		string = '['+ string +']'
	return string

def suggestion_juniper(hostname, acl_name, term_numb, subnet):
	"""
	generate addition lines to junos filter configuration
	rtype: list
	"""
	sugg = ['----- '+ hostname, 'conf priv']
	sugg.append( 'del firewall family inet filter '+ acl_name +' term EXPLICIT_DENY then discard')
	for flow in subnet.flows:
		if (not iplib.is_ips_in_net(flow.src.ip, subnet.ip, subnet.pref) and \
			not iplib.is_ips_in_net(flow.dst.ip, subnet.ip, subnet.pref)):
			continue
		src_port = port_in_juniper_notation( flow.src.port )
		dst_port = port_in_juniper_notation( flow.dst.port )
		acl_prefix = 'set firewall family inet filter '+ acl_name +' term '+ term_numb
		if (iplib.is_ips_in_net(flow.src.ip, subnet.ip, subnet.pref)):
			for src_ip in flow.src.ip:
				sugg.append( acl_prefix + ' from source-address '+ src_ip +'/'+ str(flow.src.pref))
			for dst_ip in flow.dst.ip:
				sugg.append( acl_prefix + ' from destination-address '+ dst_ip +'/'+ str(flow.dst.pref))
			sugg.append( acl_prefix + ' from protocol '+ flow.src.prot)
			sugg.append( acl_prefix + ' from source-port '+ src_port )
			sugg.append( acl_prefix + ' from destination-port '+ dst_port )
		if (iplib.is_ips_in_net(flow.dst.ip, subnet.ip, subnet.pref)):
			for dst_ip in flow.dst.ip:
				sugg.append( acl_prefix + ' from source-address '+ dst_ip +'/'+ str(flow.dst.pref))
			for src_ip in flow.src.ip:
				sugg.append( acl_prefix + ' from destination-address '+ src_ip +'/'+ str(flow.src.pref))
			sugg.append( acl_prefix + ' from protocol '+ flow.src.prot)
			sugg.append( acl_prefix + ' from source-port '+ dst_port )
			sugg.append( acl_prefix + ' from destination-port '+ src_port )
		sugg.append( acl_prefix + ' then accept')
		term_numb = next_term([term_numb])
	sugg.append( 'set firewall family inet filter '+ acl_name +' term EXPLICIT_DENY then discard')
	sugg.append( 'show | compare')
	sugg.append( 'commit check')
	sugg.append( 'commit and-quit')
	return sugg

def suggestion_cisco(hostname, acl_name, subnet):
	"""
	generate addition lines to cisco access-list configuration
	rtype: list
	"""
	sugg = [hostname, acl_name]
	req = digger.read_request_file()
	sugg.append('!  | smalgin '+ datetime.datetime.strftime (datetime.datetime.now(), "%d.%m.%y"))
	for flow in subnet.flows:
		src_port_list = re.split('[.,;]', flow.src.port )
		dst_port_list = re.split('[.,;]', flow.dst.port )
		for src_port in src_port_list:
			for dst_port in dst_port_list:
				src_port_pref = add_cisco_prefix_to_port(src_port)
				dst_port_pref = add_cisco_prefix_to_port(dst_port)
				if (iplib.is_ips_in_net(flow.src.ip, subnet.ip, subnet.pref)):
					for src_ip in flow.src.ip:
						for dst_ip in flow.dst.ip:
							sugg.append(' permit '+ flow.src.prot + src_ip + src_port_pref +\
							            ' host '+ dst_ip + dst_port_pref)
				if (iplib.is_ips_in_net(flow.dst.ip, subnet.ip, subnet.pref)):
					for src_ip in flow.src.ip:
						for dst_ip in flow.dst.ip:
							sugg.append(' permit '+ flow.src.prot + dst_ip + dst_port_pref +\
							            ' host '+ src_ip + src_port_pref)
	sugg.append('!')
	return sugg

def prn_info (( hostname, iface, iface_cont, acl, acl_cont, acl_add )) :
	"""
	print info digged from the origin
	"""
	print '\n\thostname '+ hostname +'\n'
	for line in iface_cont:
		print '\t'+ line
	print
	for line in acl_cont:
		print '\t'+ line
	print '\n\nsuggestion:\n'
	for line in acl_add:
		print line
	print

def ask_origin(subnet):
	"""
	dig info from netnode about connected interface, access-list/filter
	"""
#	session = digger.login_hotrod()
	session = digger.Session()
#	hostname, vendor = digger.login_netnode(session, subnet.orig)
	session.login_netnode(subnet.orig)
	subnet.set_vrf( digger.correct_vrf( subnet.vrf, session ))
	iface, iface_cont, acl, acl_cont, acl_add = \
		get_origination_info(session, subnet)
#	session.sendline('exit')
#	session.sendline('exit')
	hostname = session.hostname
	del session
	return hostname, iface, iface_cont, acl, acl_cont, acl_add


if __name__ == "__main__":
	iphost, ipnet, prefix, originator, vpn = parse_args()
	request = digger.read_request_file()
	subnet = digger.SubNet(ipnet, prefix, request, [originator], [vpn])
	subnet.pick_vrf()
	prn_info( ask_origin( subnet ))

