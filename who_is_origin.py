#!/usr/bin/env python

''' dig information about host's ip address '''

import pexpect
import re
import sys
import digger

def longest_prefix(strings):
	''' extract net and longest prefix from list of strings on junos
	rtype: (string, int) '''
	longest = 0
	net = ''
	for line in strings:
		match = re.search('^(\d+\.\d+\.\d+\.\d+)\/(\d+)\s+', line)
		if match:
			pref = int(match.group(2))
			if (pref > longest) and (pref != 32):
				longest = pref
				net = match.group(1)
	return net, longest

def extract_vpn_name(string):
	''' exract vpn name from string on junos
	rtype: string '''
	match = re.search('^(\S+).inet.0:', string)
	if match:
		return match.group(1)
	return 'ERROR in extract_vpn_name routine'

def extract_vpn_list(strings, net, pref):
	''' extract vpns name in which net/pref exists from list of strings on junos
	rtype: list of strings'''
	vpns = []
	for i in range(len(strings)):
		match = re.search('^(\d+\.\d+\.\d+\.\d+)\/(\d+)\s+', strings[i])
		if match:
			if ((match.group(1) == net) and (int(match.group(2)) == pref)):
				vpns.append(extract_vpn_name(strings[i-3]))
	return vpns

def extract_next_hops(strings, vpns):
	''' extract protocol next-hops for corresponded vpns from strings 
	rtype: list of strings'''
	next_hops = ['ERROR in extract_next_hops routine'] * len(vpns)
	indx_curr_vpn = 0
	for line in strings:
		match = re.search('^(\S+).inet.0:', line )	# looking for a VPN name
		if match:
			for i in range(len(vpns)):
				if vpns[i] == match.group(1):
					indx_curr_vpn = i
			continue
		match = re.search('^\s+Protocol next hop: (\d+\.\d+\.\d+\.\d+)\s*$', line )	# looking for a next hop
		if match:
			next_hops[ indx_curr_vpn ] = match.group(1)
		match = re.search('^\s+Next hop: via (\S+), selected\s*$', line )	# connected
		if match:
			next_hops[ indx_curr_vpn ] = '10.78.104.207'
	return next_hops

def prn_info(ipnet, prefix, vpns, nexthops):
	''' print digged information '''
	print '\n\tcomprising network '+ str(ipnet) + '/' + str(prefix) +' originates from'
	print '\t\tvpn\t\torigin'
	for i in range(len(vpns)):
		print '\t\t'+ vpns[i] +'\t'+ nexthops[i]
	print

def print_suggestion(iphost, ipnet, prefix, vpns, nexthops):
	''' print suggestion about next-step action'''
	if len(vpns) == 1:
		print '\thint: ./ask_origin.py '+ iphost +' '+ipnet +'/'+ str(prefix) +' '+ nexthops[0] +' '+ vpns[0] +'\n'
		return
	if 'V3-BILLING03-00' in vpns:
		key = vpns.index('V3-BILLING03-00')
		print '\thint: ./ask_origin.py '+ iphost +' '+ ipnet +'/'+ str(prefix) +' '+ nexthops[key] +' V3\n'
		return
	if 'V20-OFFICE20-00' in vpns:
		key = vpns.index('V20-OFFICE20-00')
		print '\thint: ./ask_origin.py '+ iphost +' '+ipnet +'/'+ str(prefix) +' '+ nexthops[key] +' V20\n'
		return


def who_is_origin(iphost):
	''' dig information about host's ip address
	rtype: (string, int, list, list)'''
#	net = ''	# net's ip address
#	prefix = 0	# subnet's prefix
#	vpns = []
#	nexthops = []
#	hostname = ''
	session = digger.Session()
	session.login_netnode('spb-ats560-pe1')
	lines = session.send('show route ' + iphost + ' active-path')
	net, prefix = longest_prefix(lines)
	vpns = extract_vpn_list(lines, net, prefix)
	lines = session.send('show route '+ net +'/'+ str(prefix) +' exact detail')
	nexthops = extract_next_hops(lines, vpns)
	del session
	return net, prefix, vpns, nexthops


if __name__ == "__main__":

	if len(sys.argv) != 2:
		print "usage: who_is_origin <host's ip address>"
		print "example, ./who_is_origin.py 10.78.20.55"
		sys.exit(0)
	iphost = sys.argv[1]

	net, prefix, vpns, nexthops = who_is_origin(iphost)

	prn_info(net, prefix, vpns, nexthops)
	print_suggestion(iphost, net, prefix, vpns, nexthops)

