#!/usr/bin/env python

''' ip address, sub-network, mask, and etc. manipulation '''

import re
import TestSuite

def is_like_ip(string):
	if type(string) != str: return False
	return re.search('\d+[.,]\d+[.,]\d+[.,]\d+', string)

def is_ip(string):
	match = re.search('(\d+)\.(\d+)\.(\d+)\.(\d+)', string)
	if match:
		return int(match.group(1)) >= 0 and int(match.group(1)) < 256 and \
			   int(match.group(2)) >= 0 and int(match.group(2)) < 256 and \
			   int(match.group(3)) >= 0 and int(match.group(3)) < 256 and \
			   int(match.group(4)) >= 0 and int(match.group(4)) < 256
	else: return False

def is_masks_octet(octet):
	return octet in [0, 128, 192, 224, 240, 248, 252, 254, 255]

def is_mask(string):
	if not is_ip(string): return False
	match = re.search('(\d+)\.(\d+)\.(\d+)\.(\d+)', string)
	return match.group(1) >= match.group(2) >= match.group(3) >= match.group(4) and \
		   is_masks_octet( int(match.group(1)) ) and \
		   is_masks_octet( int(match.group(2)) ) and \
		   is_masks_octet( int(match.group(3)) ) and \
		   is_masks_octet( int(match.group(4)) )

def ip_to_int(ip):
	if type(ip) == str:
		match = re.search('(\d+)\.(\d+)\.(\d+)\.(\d+)', ip)
		if match:
			return int(match.group(1)) * 16777216 + \
				   int(match.group(2)) * 65536 + \
				   int(match.group(3)) * 256 + \
				   int(match.group(4))

def is_subnets(iplist, mask):	# Whether all ip in iplist are subnets
	pref = mask_to_pref(mask)
	wc_int = 4294967295 >> int( pref )	# 2**32-1
	ret = True
	for ip in iplist:
		ip_int = ip_to_int(ip)
		ret = ret and ((ip_int & wc_int) == 0)
	return ret

def mask_to_pref(string):
	match = re.search('(\d+)\.(\d+)\.(\d+)\.(\d+)', string)
	mask = 0
	for i in range(4):
		mask += 256**i * int(match.group(i + 1))
	return bin(mask)[2:].count('1')

def pref_to_wildcard( pref ):	# return wildcard'd string representation of prefix
	wc_i = 4294967295 >> int( pref )	# 2**32-1
	s = '';
	for i in range( 4 ):
		s += str( wc_i >> ( (3-i)*8 ) & 255 )
		if i <> 3:
			s += '.'
	return s

def prefix_to_mask( pref ):	# return mask's string representation of prefix
	mask_i = 4294967296 - (1 << (32-pref))	# int value of the mask
	s = '';
	for i in range( 4 ):
		s += str (( mask_i >> (3-i)*8 ) & 255 )
		if i <> 3:
			s += '.'
	return s

def is_ip_in_net(host_ip, net_ip, pref):
	""" is host_ip contains in net_ip/pref """
	mask_i = 4294967296 - (1 << (32-pref))	# int value of the mask
	return ( ip_to_int(host_ip) & mask_i ) == ip_to_int(net_ip)

def is_ips_in_net(host_ips, net_ip, pref):
	""" is any host_ip from host_ips list contains in net_ip/pref """
	ret = False
	for host_ip in host_ips:
		ret = ret or is_ip_in_net(host_ip, net_ip, pref)
	return ret

if __name__ == "__main__":
	ts = TestSuite.TestSuite()
	ts.asEQ( is_subnets( ['10.78.184.0'], '255.255.255.0' ), True, 'test 1.1' )
	ts.asEQ( is_subnets( ['10.78.184.32'], '255.255.255.224' ), True , 'test 1.2' )
	ts.asEQ( is_subnets( ['10.78.184.32'], '255.255.255.255' ), True , 'test 1.3' )
	ts.asEQ( is_subnets( ['10.78.184.33'], '255.255.255.240' ), False , 'test 1.4' )
	ts.asEQ( is_subnets( ['10.78.184.32', '10.78.184.33'], '255.255.255.240' ),
		False , 'test 1.5' )
	ts.asEQ( is_subnets( ['10.78.184.32', '10.78.184.48'], '255.255.255.240' ),
		True , 'test 1.6' )

	ts.asEQ( is_ip_in_net('10.0.0.1', '10.0.0.0', 24), True, 'test 2.1')
	ts.asEQ( is_ip_in_net('10.0.1.0', '10.0.0.0', 24), False, 'test 2.2')
	ts.asEQ( is_ip_in_net('10.1.1.1', '0.0.0.0', 0), True, 'test 2.3')
	ts.asEQ( is_ip_in_net('10.0.0.1', '10.0.0.1', 32), True, 'test 2.4')
	ts.asEQ( is_ip_in_net('10.0.0.48', '10.0.0.32', 28), False, 'test 2.5')
	ts.tally()

