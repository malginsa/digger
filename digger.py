#!/usr/bin/env python
""" main library for digging """

import iplib
import glob
import os
import openpyxl
import pexpect
import re
import sys
import TestSuite

#session = None
#"""
#Hmm.. a global variable?! Yes, a global. It's a pointer to the structure, 
#describing current virtual terminal session. This session can be only one
#and it is used throughout all modules.
#"""

def correct_vrf(vrf, session):
	""" correct vrf name of certain netnode,
	by changing to/appending appropriate suffix to the root of vrf
	rtype: string"""
	match = re.search('(V\d+)[-:]', vrf)
	if match: root = match.group(1)
	else: root = vrf
	info = []
	if session.vendor == 'juniper':
		info = session.send('show route instance brief')
	if session.vendor == 'cisco':
		info = session.send('sh ip vrf brief')
	for line in info:
		match = re.search(root+'(\S+)\s', line)
		if match:
			root = root + match.group(1)
			break
	return root
	
def extract_ip_list(string):	# dedicated to Kondratyev Alexey
	""" extract list of ip addresses from string """
	ip_list = re.findall('(\d+[.,]\d+[.,]\d+[.,]\d+)', string)
	ret = []
	for item in ip_list:
		ip_str = re.sub(',', '.', item)
		if iplib.is_ip( ip_str ): ret.append( ip_str )
	return ret

def ip_list_to_str(lst):
	""" return string representation of list """
	if len(lst) == 1: return lst[0]
	return '['+ ' '.join(lst) + ']'

def filename_extension(filename):
	""" return extension of filename """
	match = re.search('.+\.([\w]+)',filename)
	if match:
		return match.group(1)
	else:
		return ''

def read_request_file(filename = None):
	"""
	read request from file, choosed by priority below
		0) parameter
		1) request.xlsx
		2) recent modified *.xlsx file
		3) request.txt
		4) recent modified *.txt file
	rtype: list of flows
	"""
	if (filename is not None):
		if (filename_extension(filename) == 'xlsx'):
			return read_xlsx_request( filename )
		elif (filename_extension(filename) == 'txt'):
			return read_txt_request( filename )
		else:
			print 'unsupported format of request-file'
			sys.exit(1)

	filenames = glob.glob('*.xlsx')
	if 'request.xlsx' in filenames:
		return read_xlsx_request('request.xlsx')
	if len(filenames) != 0:
		imax = 0
		for i in range(1,len(filenames)):
			if os.path.getmtime(filenames[i]) > os.path.getmtime(filenames[imax]):
				imax = i
		return read_xlsx_request( filenames[imax] )

	filenames = glob.glob('*.txt')
	if 'request.txt' in filenames:
		return read_txt_request('request.txt')
	if len(filenames) != 0:
		imax = 0
		for i in range(1,len(filenames)):
			if os.path.getmtime(filenames[i]) > os.path.getmtime(filenames[imax]):
				imax = i
		return read_txt_request( filenames[imax] )

def read_xlsx_request(fname):
	""" read request from xlsx file fname; return list of flows """
	request = []
	wb = openpyxl.load_workbook(filename = fname)
	sheet_names = wb.get_sheet_names()
	ws = wb[sheet_names[0]]	# worksheet 'Template'
	for row in ws.iter_rows():
		field = []
		for cell in row:
			if type(cell.value) == unicode:
				field.append( cell.value.encode('ascii','ignore') )
			else:
				field.append( str(cell.value) )
		if iplib.is_like_ip( field[0] ):
			if iplib.is_like_ip( field[4] ):	# insert tcp in protocol-missing request
				field = field[:4] +['tcp']+ field[4:]
				print '\ttcp inserted'
			request.append(Flow(field))
	print '\nrequest has been read from '+ fname
	return request

def read_txt_request(fname):
	""" read request from txt file fname; return list of flows """
	request = []
	reqfile = open(fname, 'r')
	for line in reqfile:
		field = line.split('\t')
		for i in range(len(field)):
			field[i] = field[i].strip(' "')
		if iplib.is_like_ip( field[0] ):
			if iplib.is_like_ip( field[4] ):	# insert tcp in protocol-missing request
				field = field[:4] +['tcp']+ field[4:]
				print '\ttcp inserted'
			request.append(Flow(field))
	print '\nrequest has been read from '+ fname
	return request

def print_request(request):
	""" print request object, readed from file """
	print '\tamount = ', Flow.amount
	for i in range(len(request)):
		print request[i]

def extract_all_ips(request):
	""" extact all uniq ip addresses from request; return list of ip addresses """
	ret = []
	for flow in request:
		for side in [flow.src, flow.dst]:
			for ip in side.ip:
				if ip not in ret:
					ret.append(ip)
	return ret


class Session:
	""" virtual terminal session and all credentials, status, etc """

	def __init__(self):
		self.vty = None	# virtual terminal session from module pexpect
		self.vendor = None	# cisco, juniper,...
		self.status = 'closed'	# status of vty: 'opened' or 'closed'
		self.hostname = None	# hostname of netnode, virtual terminal session is opened on
		self.dnsname = None	# dns-name of netnode, it is argument of login_netnode()
			# available values: None, 'hotrod', '<netnode-name>'

		self.__parse_config()
		self.log = open('.session.log', 'w', 0)	# log vty
		self.__login_hotrod()

	def __parse_config(self):
		""" parse .config file """
		self.quark_transit = None
		self.username = None
		self.hotrod_password = None
		self.quark_password = None
		self.netnode_password = None
		try:
			conf = open('.config', 'r')
		except IOError:
			print "Can't open .config file"
			sys.exit(1)
		else:
			for line in conf:
				line = re.sub('=', ' ', line)
				match = re.search('quark_transit(.+)', line)
				if match:
					rest = match.group(1)
					self.quark_transit = rest.strip().lower()
				match = re.search('username(.+)', line)
				if match:
					rest = match.group(1)
					self.username = rest.strip()
				match = re.search('hotrod_password(.+)', line)
				if match:
					rest = match.group(1)
					self.hotrod_password = rest.strip()
				match = re.search('quark_password(.+)', line)
				if match:
					rest = match.group(1)
					self.quark_password = rest.strip()
				match = re.search('netnode_password(.+)', line)
				if match:
					rest = match.group(1)
					self.netnode_password = rest.strip()
			conf.close()
		if (self.quark_transit is None or \
			 self.username is None or \
			 self.hotrod_password is None or \
			 self.quark_password is None or \
			 self.netnode_password is None):
			print ".config file incorrect"
			sys.exit(1)

	def __login_hotrod(self):
		""" log in to hotrod using virtual terminal session self.vty """
		if self.quark_transit == 'yes':
			self.vty = pexpect.spawn('ssh '+ self.username +'@quark')
			self.vty.logfile = self.log
			self.vty.expect('Password:', timeout = 6)
			self.vty.sendline(self.quark_password)
			self.vty.expect('[#\$] ', timeout = 3)
			self.vty.sendline('ssh '+ self.username +'@hotrod')
		else:
			self.vty = pexpect.spawn('ssh '+ self.username +'@hotrod')
			self.vty.logfile = self.log
		self.vty.expect('password: ', timeout = 3)
		self.vty.sendline(self.hotrod_password)
		self.vty.expect('[#\$] ', timeout = 3)
		self.status = 'opened'
		self.hostname = 'hotrod'

	def __logout_netnode(self):
		""" logout from netnode to hotrod """
		if self.status == 'closed':
			raise Exception('attempt to logout from netnode when session is closed')
		if self.hostname == 'hotrod':
			return
		lines = self.send('exit')
		match = re.search('hotrod',''.join(lines))
		while match is None:
			lines = self.send('exit')
			match = re.search('hotrod',''.join(lines))
		self.hostname == 'hotrod'

	def login_netnode(self, name):
		""" login to netnode name using virtual terminal session self.vty """
		if self.status == 'closed':
			raise Exception('attempt to login to '+ name +' using closed session')
		if self.dnsname == name:
			return
		self.dnsname = name
		if self.hostname != 'hotrod':
			self.__logout_netnode()

		self.vty.sendline('telnet ' + name)
		self.vty.expect(['login: ', 'Username: '])
		self.vty.sendline(self.username)
		self.vty.expect('Password:', timeout = 3)
		self.vty.sendline(self.netnode_password)
	
		indx = self.vty.expect(['\r\n\S+#', '\r\n'+ self.username +'@\S+> '], timeout = 3)
	
		if indx == 0:	# cisco
			match = re.search('\r\n(\S+)#', self.vty.after)
			if match:
				self.vendor = 'cisco'
				self.hostname = match.group(1)
				lines = self.send('term width 512')
				lines = self.send('term length 0')
	
		if indx == 1:	# juniper
			match = re.search('\r\n'+ self.username +'@(\S+)> ', self.vty.after)
			if match:
				self.vendor = 'juniper'
				self.hostname = match.group(1)
				lines = self.send('set cli screen-width 1024')
				lines = self.send('set cli screen-length 10000')
				lines = self.send('set cli idle-timeout 1')

	def send(self, cmd):
		""" send command cmd, return list of router's output """
		prompts = []
#		prompts.append('---\(more\s?\d*%?\)---')
#		prompts.append(' --More-- ')
		prompts.append('@'+ self.hostname +'> ')
		prompts.append( self.hostname +'#')
		prompts.append( 'hotrod' )

		self.vty.sendline(cmd)
		indx = self.vty.expect( prompts, timeout = 7)
		output = self.vty.before + self.vty.after

#		while (indx == 0) or (indx == 1):
#			self.vty.send(' ')
#			indx = self.vty.expect( prompts, timeout = 7)
#			output += self.vty.before
		return output.split('\r\n')

	def __del__(self):
		if self.status == 'closed':
			raise Exception('attempt to close closed session')
		if self.hostname != 'hotrod':
			self.__logout_netnode()
		self.vty.sendline('exit')
		if self.quark_transit == 'yes':
			self.vty.sendline('exit')
		self.log.write('vty session closed normally\n')
		self.log.close()


class SubNet:
	""" digged subnetwork information """
	def __init__(self, ip, pref, request, posorig=[], posvpn=[]):
		'in request appropriate var snet is to be updated; in __add_sides() method'
		self.ip = ip		# ip address of this subnet
		self.pref = pref	# length of prefix of this subnet
		self.orig = None	# true originator; string representation of ip address
		self.vrf = None	# a vpn in which the subnet is resided
		self.hostname = None	# hostname of netnode
		self.iface = None	# connected interface name
		self.iface_cont = None	# connected interface content
		self.acl = None	# access-list's/filter's name if exists
		self.acl_cont = None	# access-list's/filter's content
		self.acl_add = None	# access-list's/filter's add on
		self.posorig = posorig	# possible originators
		self.posvpn = posvpn	# possible vpns
		self.flows = []	# sides, whose ip is contained in this subnetwork
		self.__add_flows(request)

	def __str__(self):

		C_GREEN = '\033[1;32;40m'
		C_NORM = '\033[0m'
		delim = C_GREEN +' | '+ C_NORM
		return delim+ self.ip + \
		       delim+'/'+ str(self.pref) + \
		       delim+ str(self.orig) + \
		       delim+ str(self.vrf) + \
		       delim+ str(self.hostname) + \
		       delim+ str(self.iface) + \
		       delim+ str(self.iface_cont) + \
		       delim+ str(self.acl) + \
		       delim+ str(self.acl_cont) + \
		       delim+ str(self.acl_add) + \
		       delim+ '['+ ' '.join(self.posorig) +']'+ \
		       delim+ '['+ ' '.join(self.posvpn) +']'+ \
		       delim+ '['+ ' '.join( [ str(s) for s in self.flows ] ) +']\n'

	def __add_flows(self, request):
		""" add every flow from request, whose(side's) ip is contained in this subnetwork  """
		for flow in request:
			for side in [flow.src, flow.dst]:
				if iplib.is_ips_in_net(side.ip, self.ip, self.pref):
					# TODO check only one ip in side.ip[0] is not correct
					self.flows.append(flow)
					side.snet = self

	def pick_vrf(self, common_vpn=None ):
		""" pick the vpn from the posvpn, preferrable common_vpn """
		self.vrf = common_vpn if common_vpn in self.posvpn else self.posvpn[0]
		self.orig = self.posorig[ self.posvpn.index(self.vrf) ]

	def add_orig_info(self, hostname, iface, iface_cont, acl, acl_cont, acl_add):
		""" add info digged from origin about interface, acl,.."""
		self.hostname = hostname
		self.iface = iface
		self.iface_cont = iface_cont
		self.acl = acl
		self.acl_cont = acl_cont
		self.acl_add = acl_add

	def set_vrf(self, vrf):
		self.vrf = vrf


class Side:
	""" src or dst participant of network interaction """

	def __init__(self, l):	# l is a list of fields of 'half-line' of request
		# default values if corresponding field can't be parsed
		self.ip   = ['ip']	# list of ip addresses
		self.pref = 32		# length of prefix
		self.port = '>1023'	# tcp or udp port
		self.desc = 'DIT'	# description field
		self.prot = 'tcp'	# protocol TODO: move from Side to Flow

		if iplib.is_like_ip( l[0] ):
			self.ip = extract_ip_list( l[0] )

		if iplib.is_mask( l[1] ):
			if iplib.is_subnets(self.ip, l[1]):	# whether ip is really subnets?
				self.pref = iplib.mask_to_pref(l[1])

		if (l[2] != '') and \
		   (l[2] != 'None') and \
		   (l[2] != "\xF5\xE7"):	# dedicated to Kondratyev Alexey
			self.port = l[2]

		if l[3] != '':
			self.descr = l[3]

		if l[4] != '':
			self.prot = l[4].lower()
			if self.prot == 'http':	# dedicated to Maxim Larionov
				self.prot = 'tcp'

	def __str__(self):

		C_GREEN = '\033[1;32;40m'
		C_NORM = '\033[0m'
		delim = C_GREEN +' '+ C_NORM
		ret = delim+ ip_list_to_str(self.ip) + \
		      delim+ '/'+ str(self.pref) + \
		      delim+ self.port + \
		      delim+ self.prot + \
		      delim
		return ret

	def add_snet(snet):
		# it's needlessly; see method add_sides() in SubNet class
		pass

class Flow:
	""" network flow consists of two sides and corresponds one meaning line of request file """
	amount = 0	# number of flows

	def __init__(self, l):	# l is a list of fields in one string from request file
		Flow.amount += 1
		self.src = Side(l[:5])
		self.dst = Side(l[5:]+[l[4]])
		self.snet = None	# subnetwork wich consists ip;
		                  # snet is added in class SubNet routine add_sides

	def __str__(self):
		ret = str(self.src) + str(self.dst)
		if self.snet is None: return ret
		else: return ret + str(self.snet.ip) +'/'+ str(self.snet.pref)

if __name__ == "__main__":

	request = read_request_file()
	print_request(request)

#	session = Session()
#	session.login_netnode('spb-ats321-pe1')
#	print '\n'.join(session.send('sh cli')) +'\n'
#	session.login_netnode('spb-ats321-pe1')
#	print '\n'.join(session.send('sh version')) +'\n'
#	del session

